from flask import Flask, request
import os
import pymysql
import pymysql.cursors

app = Flask(__name__)

@app.route('/')
@app.route('/hello')
def hello():
    return 'Hello World !'

@app.route('/query')
def appInfo():
    conn = pymysql.connect(host='us-cdbr-east-05.cleardb.net',
                           user='bf78ffc965d109',
                           password='d1327123',
                           db='heroku_448e8780cc21c69',
                           charset='utf8mb4',
                           cursorclass=pymysql.cursors.DictCursor)
    msg = ""
    with conn.cursor() as cursor:
        sql = "select * from app_info;"
        cursor.execute(sql)
        records = cursor.fetchall()
        print("records => ", records)
        for r in records:
            print("r => ", r)
            msg += "{} , {} , {} , {} , {} <br>".format(
                r["id"], r["name"], r["version"], r["author"], r["remark"])
        print("msg => ", msg)
    return msg


if __name__ == '__main__':
    app.run('0.0.0.0', debug=True)
