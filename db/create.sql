-- 應用程式資訊
create table app_info (
    id int auto_increment primary key ,
    name    nvarchar(50),
    version nvarchar(30),
    author  nvarchar(50),
    remark  nvarchar(100)
);

-- 使用者資訊
create table user_info (
    id int auto_increment primary key ,
    name    nvarchar(50),
    account    nvarchar(30),
    password    nvarchar(20),
    photo  nvarchar(20)
);

-- 部落格文章
create table article (
    id int auto_increment primary key ,
    title    nvarchar(200),
    content    nvarchar(1200),
    author    nvarchar(30),
    date    datetime  
);

