-- 應用程式資訊
insert into app_info(name,version,author,remark) values
('JavaProjSE-v1.0.3','1.0.3','Enoxs','Java Project Simple Example - Version 1.0.3'),
('JUnitSE','1.0.2','Enoxs','Java Unit Test Simple Example'),
('SpringMVC-SE','1.0.2','Enoxs','Java Web Application Spring MVC');

-- 使用者資訊
insert into user_info(id , name , account , password , photo) values
(1,'開發者','DevAuth','0000', 'DevAuth.png'),
(2,'測試員','Demo','1234', 'Demo.jpg');

-- 部落格文章
insert into article(id , title , content , author , date ) values
(1,'PythonFlask','Text Text Text', 'DevAuth' , '2022-01-20'),
(2,'PythonSQL','CURL : Select Insert Update Delete', 'DevAuth' , '2022-01-21' ),
(3,'PythonOCR','Image to Text', 'Demo' , '2022-01-22' );
