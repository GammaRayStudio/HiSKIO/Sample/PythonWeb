from main import db

class UserInfo(db.Model):
    id = db.Column('id', db.Integer, primary_key=True)
    name = db.Column(db.String)
    account = db.Column(db.String)
    password = db.Column(db.String)
    photo = db.Column(db.Integer)

    def __init__(self, name, account , password , photo):
        self.name = name
        self.account = account
        self.password = password
        self.photo = photo

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Article(db.Model):
    id = db.Column('id', db.Integer, primary_key=True)
    title = db.Column(db.String)
    content = db.Column(db.String)
    author = db.Column(db.String)
    date = db.Column(db.Date)

    def __init__(self, title, content , author , date):
        self.title = title
        self.content = content
        self.author = author
        self.date = date

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Message(db.Model):
    id = db.Column('id', db.Integer, primary_key=True)
    article_id = db.Column(db.Integer)
    message = db.Column(db.String)
    author = db.Column(db.String)
    date = db.Column(db.Date)

    def __init__(self, article_id , message , author , date):
        self.article_id = article_id
        self.message = message
        self.author = author
        self.date = date

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
